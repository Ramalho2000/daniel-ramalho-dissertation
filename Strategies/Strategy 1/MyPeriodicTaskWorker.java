public class MyPeriodicTaskWorker extends Worker {
    private static final String WORK_RESULT = "work_result";
    
    public MyPeriodicTaskWorker(@NonNull Context context, 
        @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {

        //get MaxBackgroundTime from SharedPreferences
        SharedPreferences prefs = getApplicationContext()
            .getSharedPreferences("maxTimeMillis", Context.MODE_PRIVATE);
        
        //IF (The Background Time is exceeded)
        if(System.currentTimeMillis() > TaskEndTime) {
         
            WorkManager.getInstance(getApplicationContext())
                .cancelAllWorkByTag(getClass().getName());

            Data failureData = new Data.Builder()
                .putString("failure_reason", "Time Exceeded").build();
            return Result.failure(failureData);
        }

        //IF (App is Running)
        if(isAppRunning()) {
            //Return a failure result "App Running"
            Data failureData = new Data.Builder()
                .putString("failure_reason", "App Running").build();
            return Result.failure(failureData);
        }
        
        //Do the Work
        while(System.currentTimeMillis() < TaskEndTime) {
            bubbleSort();
        }

        //Cancel all workers with this TAG
        WorkManager.getInstance(getApplicationContext())
            .cancelAllWorkByTag(getClass().getName());

        //Return the result of the work "Success"
        Data outputData = new Data.Builder().putString(WORK_RESULT, "Success").build();
        return Result.success(outputData);

    }

    public void bubbleSort(){
        int i, n = 10000; //lenght of arr
        int[] arr = new int[n];
        int min = 0;
        int max = 100000;

        //fill array "arr" with random numbers
        for (i = 0; i < arr.length; i++)
            arr[i] = ThreadLocalRandom.current().nextInt(min, max + 1);

        for (i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (arr[j] > arr[j + 1]) {
                    // swap arr[j+1] and arr[j]
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
    }

    private boolean isAppRunning() {
        SharedPreferences prefs = getApplicationContext()
            .getSharedPreferences("setVisible", Context.MODE_PRIVATE);
        return prefs.getBoolean("setVisible", false);

    }
}
