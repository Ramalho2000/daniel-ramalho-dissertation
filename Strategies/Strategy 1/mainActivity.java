@Override
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    /*
    Your code
    */

    initBackgroundTasks();

    /*
    Your code
    */
}

@Override
protected void onPause() {
    super.onPause();

    /*
    Your code
    */

    SharedPreferences prefs = getApplicationContext().getSharedPreferences("setVisible", Context.MODE_PRIVATE);
    prefs.edit().putBoolean("setVisible", false).apply();
}

@Override
protected void onResume() {
    super.onResume();
    
    /*
    Your code
    */

    SharedPreferences prefs = getApplicationContext().getSharedPreferences("setVisible", Context.MODE_PRIVATE);
    prefs.edit().putBoolean("setVisible", true).apply();
}

public void initBackgroundTasks(){
    Context context = getApplicationContext();

    // set up worker input data
    int backgroundMaxTimeMinutes = 30;  //change this value
    long backgroundMaxTimeMillis = 60 * 1000 * backgroundMaxTimeMinutes;
    long maxTimeMillis = System.currentTimeMillis() + backgroundMaxTimeMillis;

    SharedPreferences prefs = context.getSharedPreferences("maxTimeMillis", Context.MODE_PRIVATE);

    prefs.edit().putLong("maxTimeMillis", maxTimeMillis).apply();
    System.out.println(prefs.getLong("maxTimeMillis", 0));

    long repeatIntervalMinutes = 15L;
    PeriodicWorkRequest myTaskRequest = new PeriodicWorkRequest.Builder(MyPeriodicTaskWorker.class, repeatIntervalMinutes, TimeUnit.MINUTES)
        .build();

    WorkManager.getInstance(context).enqueue(myTaskRequest);
}
