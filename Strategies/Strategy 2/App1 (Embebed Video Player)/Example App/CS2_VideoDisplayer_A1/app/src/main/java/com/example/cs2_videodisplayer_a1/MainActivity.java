package com.example.cs2_videodisplayer_a1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button watchButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        watchButton = findViewById(R.id.button);

        watchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VideoPlayerFragment videoPlayerFragment = new VideoPlayerFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, videoPlayerFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                //hide the button
                watchButton.setVisibility(View.INVISIBLE);
            }
        });

    }

    //show the button
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        watchButton.setVisibility(View.VISIBLE);
    }
}