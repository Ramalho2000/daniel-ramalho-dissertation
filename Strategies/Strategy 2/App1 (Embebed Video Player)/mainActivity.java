watchButton.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        VideoPlayerFragment videoPlayerFragment = new VideoPlayerFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, videoPlayerFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        //hide the button
        watchButton.setVisibility(View.INVISIBLE);
    }
});
