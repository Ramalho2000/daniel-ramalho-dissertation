package com.example.cs2_videodisplayer_a2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button watchButton = findViewById(R.id.button);

        watchButton.setOnClickListener(v -> {
            //Display video
            String url = "https://www.youtube.com/watch?v=YLslsZuEaNE"; //1 Minute Video - Doggie
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        });
    }
}