package com.example.challenge1;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class Fragment2 extends Fragment {

    private View rootView;
    private EditText ownerText;
    private EditText nameText;
    private EditText ageNumber;
    private  IMainActivity IMA;
    private SharedViewModel viewModel;

    public Fragment2() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //Log.i("FRAG 2", "ENTREI ONCREATE");
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Log.i("FRAG 2", "ENTREI VIEW");

        if (getActivity() instanceof MainActivity) {
            IMA = (IMainActivity) getActivity();
        }

        viewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);
        rootView = inflater.inflate(R.layout.fragment_2, container, false);

        ownerText = rootView.findViewById(R.id.ownerText);
        nameText = rootView.findViewById(R.id.nameText);
        ageNumber = rootView.findViewById(R.id.ageNumber);
        Button button2 = rootView.findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] new_values = new String[]{ownerText.getText().toString(), nameText.getText().toString(), ageNumber.getText().toString()};
                viewModel.setValueAtIndex(viewModel.getSelected(), new_values);
                IMA.onClickB2();
            }
        });

       viewModel.public_data.observe(getViewLifecycleOwner(), item -> {
            ownerText.getText().clear();
            nameText.getText().clear();
            ageNumber.getText().clear();
            ownerText.setText(item.get(viewModel.getSelected())[0]);
            nameText.setText(item.get(viewModel.getSelected())[1]);
            ageNumber.setText(item.get(viewModel.getSelected())[2]);
        });

        return rootView;
    }

}