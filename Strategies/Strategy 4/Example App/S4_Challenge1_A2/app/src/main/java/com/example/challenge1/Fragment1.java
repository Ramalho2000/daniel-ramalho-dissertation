package com.example.challenge1;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;


public class Fragment1 extends Fragment {
    private View rootView;
    private int selected = 0;
    private  IMainActivity IMA;



    public Fragment1() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //Log.i("FRAG 1", "ENTREI ONCREATE");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Log.i("FRAG 1", "ENTREI VIEW");

        if (getActivity() instanceof MainActivity) {
            IMA = (IMainActivity) getActivity();
        }

        SharedViewModel viewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);

        rootView = inflater.inflate(R.layout.fragment_1, container, false);
        Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner);
        ImageView image = rootView.findViewById(R.id.imageView);
        TextView ownerView = rootView.findViewById(R.id.ownerView);
        TextView nameView = rootView.findViewById(R.id.nameView);
        TextView ageView = rootView.findViewById(R.id.ageView);

        Button button = rootView.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IMA.onClickB1();
            }
        });
        viewModel.public_data.observe(getViewLifecycleOwner(), item -> {
            ownerView.setText(item.get(selected)[0]);
            nameView.setText(item.get(selected)[1]);
            ageView.setText(item.get(selected)[2]);
        });


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //Log.i("FRAG 1", "NEW SELECTED");
                selected = spinner.getSelectedItemPosition();
                viewModel.setSelected(selected);

                //Change values according to selection
                ownerView.setText(viewModel.getValueAtIndex(selected)[0]);
                nameView.setText(viewModel.getValueAtIndex(selected)[1]);
                ageView.setText(viewModel.getValueAtIndex(selected)[2]);
                switch (selected){
                    case 0:
                        image.setImageResource(R.drawable.frog);
                        break;
                    case 1:
                        image.setImageResource(R.drawable.rhino);
                        break;
                    case 2:
                        image.setImageResource(R.drawable.snail);
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        return rootView;
    }
}