package com.example.challenge1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import android.os.Bundle;

import java.util.concurrent.ThreadLocalRandom;


public class MainActivity extends AppCompatActivity implements IMainActivity{

    private FragmentManager manager;
    private Fragment1 f1;
    private Fragment2 f2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        manager = getSupportFragmentManager();

        //Call thread order array
        Thread t1 = new OrderArrayThread();
        Thread t2 = new OrderArrayThread();

        t1.start();
        t2.start();

        f1 = new Fragment1();
        f2 = new Fragment2();

        //manager.beginTransaction().replace(R.id.firstLayout, f1, f1.getTag()).commit();
        manager.beginTransaction().add(R.id.firstLayout, f1, f1.getTag()).commit();
    }


    public void onClickB1() {
        manager.beginTransaction().replace(R.id.firstLayout, f2, f2.getTag()).commit();
        //manager.beginTransaction().add(R.id.firstLayout, f2, f2.getTag()).commit();
    }
    public void onClickB2() {
        manager.beginTransaction().replace(R.id.firstLayout, f1, f1.getTag()).commit();
        //manager.beginTransaction().add(R.id.firstLayout, f1, f1.getTag()).commit();
    }

    class OrderArrayThread extends Thread {
        @Override
        public void run() {
            while(true) {
                bubbleSort();
            }
        }
    }

    public void bubbleSort(){
        int i, n = 10000; //lenght of arr
        int[] arr = new int[n];
        int min = 0;
        int max = 100000;

        //fill array "arr" with random numbers
        for (i = 0; i < arr.length; i++)
            arr[i] = ThreadLocalRandom.current().nextInt(min, max + 1);

        for (i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (arr[j] > arr[j + 1]) {
                    // swap arr[j+1] and arr[j]
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
    }
}