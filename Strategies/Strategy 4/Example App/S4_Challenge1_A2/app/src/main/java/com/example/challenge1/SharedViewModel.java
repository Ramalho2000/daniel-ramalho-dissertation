package com.example.challenge1;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;


public class SharedViewModel extends ViewModel {

    private MutableLiveData<List<String[]>> data = new MutableLiveData<List<String[]>>();
    public LiveData<List<String[]>> public_data = data;

    private MutableLiveData<Integer> selected = new MutableLiveData<Integer>();
    public LiveData<Integer> public_selected = selected;

    public SharedViewModel(){

        List<String[]>  values = new ArrayList<String[]>();
        values.add(new String[]{"Frog owner", "Frog name", "1"});
        values.add(new String[]{"Rhino owner", "Rhino name", "2"});
        values.add(new String[]{"Snail owner", "Snail name", "3"});

        data.setValue(values);
    }

    public String[] getValueAtIndex(final int the_index) throws IndexOutOfBoundsException{
        return data.getValue().get(the_index);
    }

    public void setValueAtIndex(final int the_index, String[] new_values) throws IndexOutOfBoundsException{
        data.getValue().set(the_index, new_values);

    }

    public Integer getSelected() {
        return selected.getValue();
    }

    public void setSelected(Integer newNumber) {
        selected.setValue(newNumber);
        //Log.i("VIEW MODEL", "ALTEREI O VALUE:" + public_selected.getValue());
    }

}
