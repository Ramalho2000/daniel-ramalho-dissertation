@Override
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    
    /*
    Your code
    */
    
    //Call thread order array
    Thread t1 = new OrderArrayThread();
    Thread t2 = new OrderArrayThread();

    t1.start();
    t2.start();

    /*
    Your code
    */
}

class OrderArrayThread extends Thread {
    @Override
    public void run() {
        while(true) {
            bubbleSort();
        }
    }
}

public void bubbleSort(){
        int i, n = 10000; //lenght of arr
        int[] arr = new int[n];
        int min = 0;
        int max = 100000;

        //fill array "arr" with random numbers
        for (i = 0; i < arr.length; i++)
            arr[i] = ThreadLocalRandom.current().nextInt(min, max + 1);

        for (i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (arr[j] > arr[j + 1]) {
                    // swap arr[j+1] and arr[j]
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
}
